# Mirai (Japanese for "future")
### A Futures layer over [Mio](https://github.com/tokio-rs/mio)

The goal of this crate is to provide what Mio has to offer, but removing the boilerplate for async 
executors, plus a few other (disabled by default) features.

## TODO:
- Unix-specific features (#1)
- Tokio Async IO trait implementations (#2)
- System-driven timers (#3)
